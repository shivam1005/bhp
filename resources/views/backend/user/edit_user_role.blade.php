
<div class="row">
	<!-- right column -->
	<div class="col-md-4 white-box">
		<!-- general form elements disabled -->
		<div class="box box-info">
			
			<div class="box-body">
				<form method="post">
					@csrf
					<input type="hidden" name="permission[]" value="dashboard" />
					<div class="form-group col-lg-12 {{ $errors->has('user_role') ? 'has-error' : ''}}">
						<label>User Role <span style="color:red;">*</span></label>
						<input type="text" class="form-control" name="user_role" value="{{ $data->role }}"  placeholder="Enter User Role"/>
					</div>

					<div class="form-group col-lg-12">
						
						<ul class="list-group">
							@php $per_data = unserialize($data->permission) @endphp
														
							<li class="list-group-item">
								<label>User Role</label><br>
								<ul class="list-group">
									<li class="list-group-item">
										<input @if(in_array('add_role', $per_data)) checked @endif type="checkbox" name="permission[]" value="add_role"> Add & List<br>
									</li>
									<li class="list-group-item">
										<input @if(in_array('edit_role', $per_data)) checked @endif type="checkbox" name="permission[]" value="edit_role"> Edit<br>
									</li>
								</ul>
							</li>
							
							<li class="list-group-item">
								<label>User</label><br>
								<ul class="list-group">
									<li class="list-group-item">
										<input @if(in_array('add_user', $per_data)) checked @endif type="checkbox" name="permission[]" value="add_user"> Add<br>
									</li>
									<li class="list-group-item">
										<input @if(in_array('all_users', $per_data)) checked @endif type="checkbox" name="permission[]" value="all_users"> List<br>
									</li>
									<li class="list-group-item">
										<input @if(in_array('edit_user', $per_data)) checked @endif type="checkbox" name="permission[]" value="edit_user"> Edit<br>
									</li>
								</ul>
							</li>
							
						</ul>
					</div>

					<div class="form-group col-lg-12">    
						<input type="submit" class="btn btn-info" value="Update">
					</div> 
				</form>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
	
</div>   <!-- /.row -->
