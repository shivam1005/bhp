<?php 
	$roles = (new \App\Helpers\Helpers)->get_all_info( 'user_role' ); 
?> 

<div class="row">
	<div class="col-sm-8">
		<div class="white-box">
			<div class="box box-info">
				<div class="box-body">
					<form autocomplete="off" method="post" enctype="multipart/form-data">
						@csrf
						<div class="row">
							<div class="form-group col-lg-4 {{ $errors->has('role_id') ? 'has-error' : ''}}">
								<label>Type <span style="color:red;">*</span></label>
								<select class="form-control" name="role_id">
									<option value="">Select Type</option>
									@forelse( $roles as $role )
										<option @php if ( $role->id == $data->role_id ) echo "selected"; @endphp value="{{ $role->id }}">{{ $role->role }}</option>
									@empty
										<option value="">No Role Found!!!</option>
									@endforelse
								</select>
								@error('role_id')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-4 {{ $errors->has('first_name') ? 'has-error' : ''}}">
								<label>First Name <span style="color:red;">*</span></label>
								<input type="text" class="form-control" name="first_name"  value="{{ $data->first_name }}" placeholder="Enter First Name"/>
								@error('first_name')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-4 {{ $errors->has('last_name') ? 'has-error' : ''}}">
								<label>Last Name <span style="color:red;">*</span></label>
								<input type="text" class="form-control" name="last_name"  value="{{ $data->last_name }}" placeholder="Enter Last Name"/>
								@error('last_name')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>	

							<div class="form-group col-lg-4 {{ $errors->has('email') ? 'has-error' : ''}}">
								<label>Email <span style="color:red;">*</span></label>
								<input type="text" class="form-control" name="email" value="{{ $data->email }}"  placeholder="Enter Email"/>
								@error('email')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>
							
							<div class="form-group col-lg-4 {{ $errors->has('phone') ? 'has-error' : ''}}">
								<label>Phone <span style="color:red;">*</span></label>
								<input type="text" class="form-control" name="phone"  value="{{ $data->phone }}" placeholder="Enter Phone"/>
								@error('phone')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-4 {{ $errors->has('password') ? 'has-error' : ''}}">
								<label>Password <span style="color:red;">*</span></label>
								<input type="Password" class="form-control" name="password"  value="" placeholder="Leave Empty if you don't want to change password"/>
								@error('password')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>
						</div>	
						
						<div class="row">
							<div class="form-group col-lg-12">   
								<input type="submit" class="btn btn-info" value="Submit">
								<input type="hidden" name="hidden_password"  value="{{ $data->password }}" />
								@error('address1')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>  
						</div>  
					</form>

				</div><!-- /.box-body -->
			</div><!-- /.box -->
			
		</div>
	</div>
</div>