
<div class="row">
	<div class="col-xs-12 white-box">
		
		<div class="box">
			<div class="box-body table-responsive">
				<table id="example1" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.No</th>
							<th>First Name</th>
							<th>Email</th>
							<th>Mobile</th>
							<th>Role</th>
							<th>Date</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i=1; @endphp
						@forelse( $users as $user )
							<tr>
								<td> {{ $i }} </td>
								<td> {{ $user->first_name }} </td>
								<td> {{ $user->email }} </td>
								<td> {{ $user->phone }} </td>

								<td>
									<label> {{ (new \App\Helpers\Helpers)->get_info("user_role", $user->role_id)->role }} </label>
								</td>
								
								<td> @php echo date("d-M-Y", strtotime( $user->created_at) ); @endphp </td>

								<td>
									@php if( $user->status == 1 ){  @endphp
										<span style="color:green;">Enabled</span> ( <a style="color:red;" href="{{ url('/users/changestatus/'.$user->id) }}">Disable</a> )
									@php } @endphp
									<?php if( $user->status == 2 ){  ?>
										<span style="color:red;">Disabled</span> ( <a style="color:green;" href="{{ url('/users/changestatus/'.$user->id) }}">Enable</a> )
									@php } @endphp
								</td>

								<td>
									<?php $enc_id = (new \App\Helpers\Helpers)->encrypt($user->id); ?>
									<div class="btn-group">
									  <button type="button" class="btn btn-info btn-flat">Action</button>
									  <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									  </button>
									  <ul class="dropdown-menu" role="menu">
										<li><a href="{{ route('edit_user', ['id' => $enc_id]) }}">Edit</a></li>
										<li><a href="{{ route('delete_user', ['id' => $enc_id]) }}">Delete</a></li>
									  </ul>
									</div>
								</td>
								
							</tr>
						@php $i++; @endphp	
						@empty
						<tr><td colspan="8">Not Found!</td></tr>
						@endforelse   
					</tbody>
					<tfoot>
						<tr>
							<th>S.No</th>
							<th>First Name</th>
							<th>Email</th>
							<th>Mobile</th>
							<th>Role</th>
							<th>Date</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</tfoot>
				</table>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
		
	</div>
</div>
