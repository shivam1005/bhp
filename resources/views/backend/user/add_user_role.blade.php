
<div class="row">
	<!-- right column -->
	<div class="col-md-4 white-box">
		
		<!-- general form elements disabled -->
		<div class="box box-info">
			
			<div class="box-body">

				<form method="post">
					@csrf
					<input type="hidden" name="permission[]" value="dashboard" />
					<div class="form-group col-lg-12 {{ $errors->has('user_role') ? 'has-error' : ''}}">
						<label>User Role <span style="color:red;">*</span></label>
						<input type="text" class="form-control" name="user_role" value=""  placeholder="Enter User Role"/>
					</div>

					<div class="form-group col-lg-12">
						<ul class="list-group">
							
							<li class="list-group-item">
								<label>User Role</label><br>
								<ul class="list-group">
									<li class="list-group-item">
										<input type="checkbox" name="permission[]" value="add_role"> Add & List<br>
									</li>
									<li class="list-group-item">
										<input type="checkbox" name="permission[]" value="edit_role"> Edit<br>
									</li>
								</ul>
							</li>
							
							<li class="list-group-item">
								<label>User</label><br>
								<ul class="list-group">
									<li class="list-group-item">
										<input type="checkbox" name="permission[]" value="add_user"> Add<br>
									</li>
									<li class="list-group-item">
										<input type="checkbox" name="permission[]" value="all_users"> List<br>
									</li>
									<li class="list-group-item">
										<input type="checkbox" name="permission[]" value="edit_user"> Edit<br>
									</li>
								</ul>
							</li>
							
						</ul>
					</div>

					<div class="form-group col-lg-12">    
						<input type="submit" class="btn btn-info" value="Submit">
					</div> 
				</form>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
	
	<div class="col-md-6 white-box">
		<!-- general form elements disabled -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title"></h3>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive">
				<table id="example1" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Name</th>
							<th>Action</th>
							<!--<th>Delete</th>--> 
						</tr>
					</thead>
					<tbody>
						@php $i=1; @endphp
						@forelse($data as $d)

						   	<tr>
						   		<td>{{ $i }}</td>	
					            <td>{{ $d->role }}</td>
					            <td>
									<?php $enc_id = (new \App\Helpers\Helpers)->encrypt($d->id); ?>
									<div class="btn-group">
									  <button type="button" class="btn btn-info btn-flat">Action</button>
									  <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									  </button>
									  <ul class="dropdown-menu" role="menu">
										<li><a href="{{ route('edit_role', ['id' => $enc_id]) }}">Edit</a></li>
										@if($d->id > 3)
										<li><a href="{{ route('delete_role', ['id' => $enc_id]) }}">Delete</a></li>
										@endif
									  </ul>
									</div>
								</td>
					        </tr>

					    @php $i++; @endphp    
						
					    @empty

    					<tr><td colspan="3">No Roll Found!!!</td></tr>	

						@endforelse
					</tbody>
					<tfoot>
						<tr>
							<th>S.No</th>
							<th>Name</th>
							<th>Action</th>
							<!--<th>Delete</th>-->
						</tr>
					</tfoot>
				</table>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!--/.col (right) -->
	
</div>   <!-- /.row -->
