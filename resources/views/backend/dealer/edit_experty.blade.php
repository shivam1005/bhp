
<div class="row">
	<!-- right column -->
	<div class="col-md-4 white-box">
		<!-- general form elements disabled -->
		<div class="box box-info">
			
			<div class="box-body">
				<form method="post">
					@csrf
					<div class="form-group col-lg-12 {{ $errors->has('experty') ? 'has-error' : ''}}">
						<label>Expert In <span style="color:red;">*</span></label>
						<input type="text" class="form-control" name="experty" value="{{ $data->expert_in }}"  placeholder="Enter Expert In"/>
					</div>

					<div class="form-group col-lg-12">    
						<input type="submit" class="btn btn-info" value="Update">
					</div> 
				</form>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
	
</div>   <!-- /.row -->
