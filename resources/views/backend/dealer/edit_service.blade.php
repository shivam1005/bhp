
<div class="row">
	<!-- right column -->
	<div class="col-md-4 white-box">
		<!-- general form elements disabled -->
		<div class="box box-info">
			
			<div class="box-body">
				<form method="post">
					@csrf
					<div class="form-group col-lg-12 {{ $errors->has('service') ? 'has-error' : ''}}">
						<label>Service Name <span style="color:red;">*</span></label>
						<input type="text" class="form-control" name="service" value="{{ $data->service_name }}"  placeholder="Enter Service"/>
					</div>

					<div class="form-group col-lg-12">    
						<input type="submit" class="btn btn-info" value="Update">
					</div> 
				</form>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
	
</div>   <!-- /.row -->
