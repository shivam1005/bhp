
<div class="row">
	<!-- right column -->
	<div class="col-md-4 white-box">
		
		<!-- general form elements disabled -->
		<div class="box box-info">
			
			<div class="box-body">

				<form method="post">
					@csrf
					<div class="form-group col-lg-12 {{ $errors->has('experty') ? 'has-error' : ''}}">
						<label>Expert In <span style="color:red;">*</span></label>
						<input type="text" class="form-control" name="experty" value=""  placeholder="Enter Expert In"/>
					</div>

					<div class="form-group col-lg-12">    
						<input type="submit" class="btn btn-info" value="Submit">
					</div> 
				</form>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
	
	<div class="col-md-6 white-box">
		<!-- general form elements disabled -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title"></h3>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive">
				<table id="example1" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Name</th>
							<th>Action</th>
							<!--<th>Delete</th>--> 
						</tr>
					</thead>
					<tbody>
						@php $i=1; @endphp
						@forelse($data as $d)

						   	<tr>
						   		<td>{{ $i }}</td>	
					            <td>{{ $d->expert_in }}</td>
					            <td>
									<?php $enc_id = (new \App\Helpers\Helpers)->encrypt($d->id); ?>
									<div class="btn-group">
									  	<button type="button" class="btn btn-info btn-flat">Action</button>
									  	<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
									  	</button>
									  	<ul class="dropdown-menu" role="menu">
											<li><a href="{{ route('edit_experty', ['id' => $enc_id]) }}">Edit</a></li>
									  	</ul>
									</div>
								</td>
					        </tr>

					    @php $i++; @endphp    
						
					    @empty

    					<tr><td colspan="3">No Roll Found!!!</td></tr>	

						@endforelse
					</tbody>
					<tfoot>
						<tr>
							<th>S.No</th>
							<th>Name</th>
							<th>Action</th>
							<!--<th>Delete</th>-->
						</tr>
					</tfoot>
				</table>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!--/.col (right) -->
	
</div>   <!-- /.row -->
