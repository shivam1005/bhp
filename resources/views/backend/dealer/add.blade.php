<?php 
	$facilities = (new \App\Helpers\Helpers)->get_all_info( 'facilities' ); 
	$experties = (new \App\Helpers\Helpers)->get_all_info( 'experties' ); 
	$services = (new \App\Helpers\Helpers)->get_all_info( 'services' ); 
?> 

<div class="row">
	<div class="col-sm-8">
		<div class="white-box">
			<div class="box box-info">
				<div class="box-body"> 
					
					<form autocomplete="off" action="#" method="post" enctype="multipart/form-data">
						@csrf
						<div class="row">
							
							<div class="form-group col-lg-6 {{ $errors->has('serive_center_name') ? 'has-error' : ''}}">
								<label>Service Center Name <span style="color:red;">*</span></label>
								<input type="text" class="form-control" name="serive_center_name"  value="" placeholder="Enter Service Center Name"/>
								@error('serive_center_name')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-6 {{ $errors->has('phone') ? 'has-error' : ''}}">
								<label>Phone <span style="color:red;">*</span></label>
								<input type="text" class="form-control" name="phone"  value="" placeholder="Enter Phone"/>
								@error('phone')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-6 {{ $errors->has('address') ? 'has-error' : ''}}">
								<label>Address <span style="color:red;">*</span></label>
								<textarea class="md-textarea form-control" name="address" rows="3"></textarea>
								@error('address')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-6 {{ $errors->has('location') ? 'has-error' : ''}}">
								<label>Location <span style="color:red;">*</span></label>
								<textarea class="md-textarea form-control" name="location" rows="3"></textarea>
								@error('location')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-4 {{ $errors->has('city') ? 'has-error' : ''}}">
								<label>City <span style="color:red;">*</span></label>
								<input type="text" class="form-control" name="city" value=""  placeholder="Enter City"/>
								@error('city')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-4 {{ $errors->has('state') ? 'has-error' : ''}}">
								<label>State <span style="color:red;">*</span></label>
								<input type="text" class="form-control" name="state" value=""  placeholder="Enter State"/>
								@error('state')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-4 {{ $errors->has('pincode') ? 'has-error' : ''}}">
								<label>Pincode <span style="color:red;">*</span></label>
								<input type="text" class="form-control" name="pincode"  value="" placeholder="Enter Pincode"/>
								@error('pincode')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-6 {{ $errors->has('authorized') ? 'has-error' : ''}}">
								<label>Authorized For <span style="color:red;">*</span></label>
								<select class="form-control" name="authorized">
									<option value="">Select Authorized For</option>
									<option value="1">Maruti</option>
								</select>
								@error('authorized')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-6 {{ $errors->has('services') ? 'has-error' : ''}}">
								<label>Services <span style="color:red;">*</span></label>
								<select class="form-control" name="services">
									<option value="">Select Services</option>
									@forelse( $services as $d )
										<option value="{{ $d->id }}">{{ $d->service_name }}</option>
									@empty
										<option value="">No Role Found!!!</option>
									@endforelse
								</select>
								@error('services')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-6 {{ $errors->has('facilities') ? 'has-error' : ''}}">
								<label>Facilities <span style="color:red;">*</span></label>
								<select class="form-control" name="facilities">
									<option value="">Select Facilities</option>
									@forelse( $facilities as $d )
										<option value="{{ $d->id }}">{{ $d->facility_name }}</option>
									@empty
										<option value="">No Role Found!!!</option>
									@endforelse
								</select>
								@error('facilities')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>

							<div class="form-group col-lg-6 {{ $errors->has('experties') ? 'has-error' : ''}}">
								<label>Experties <span style="color:red;">*</span></label>
								<select class="form-control" name="experties">
									<option value="">Select Experties</option>
									@forelse( $experties as $d )
										<option value="{{ $d->id }}">{{ $d->expert_in }}</option>
									@empty
										<option value="">No Role Found!!!</option>
									@endforelse
								</select>
								@error('experties')
									<span class="help-block">{{ $message }}</span>
								@enderror
							</div>
							
						</div>	
						
						<div class="row">
							<div class="form-group col-lg-12">    
								<input type="submit" class="btn btn-info" value="Submit">
							</div>  
						</div>  
					</form>

				</div><!-- /.box-body -->
			</div><!-- /.box -->
			
		</div>
	</div>
</div>