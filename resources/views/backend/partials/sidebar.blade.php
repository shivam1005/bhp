<?php 
  $user_data = (new \App\Helpers\Helpers)->get_info( 'users', Session::get('id') );  
?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('public/backend/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ $user_data->first_name.' '.$user_data->last_name}}</p>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        
        <li class="header">MAIN NAVIGATION</li>
        
        <li>
          <a href="{{ route('dashboard') }}">
            <i class="fa fa-th"></i> <span>Dashboard</span>
          </a>
        </li>
		
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-briefcase"></i>
            <span>Dealer</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('add_dealer') }}"><i class="fa fa-angle-double-right"></i> Add Dealer</a></li>
            <li><a href="{{ route('all_dealer') }}"><i class="fa fa-angle-double-right"></i> All Dealers</a></li>
            <li><a href="{{ route('add_service') }}"><i class="fa fa-angle-double-right"></i> Add Service</a></li>
            <li><a href="{{ route('add_experty') }}"><i class="fa fa-angle-double-right"></i> Add Experty</a></li>
            <li><a href="{{ route('add_facility') }}"><i class="fa fa-angle-double-right"></i> Add Facility</a></li>
          </ul>
        </li>
	
		<!--<li class="treeview">
          <a href="#">
            <i class="fa fa-money"></i>
            <span>Expenses</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/expense/add') }}"><i class="fa fa-angle-double-right"></i> Add</a></li>
            <li><a href="{{ url('/expense/list') }}"><i class="fa fa-angle-double-right"></i> List</a></li>
          </ul>
        </li>

		<li class="treeview">
          <a href="#">
            <i class="fa fa-user-circle"></i>
            <span>Passengers</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/passenger/add') }}"><i class="fa fa-angle-double-right"></i> Add</a></li>
            <li><a href="{{ url('/passenger/list') }}"><i class="fa fa-angle-double-right"></i> List</a></li>
          </ul>
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-plane"></i>
            <span>Planes</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/planes/add') }}"><i class="fa fa-angle-double-right"></i> Add</a></li>
            <li><a href="{{ url('/planes/list') }}"><i class="fa fa-angle-double-right"></i> List</a></li>
          </ul>
        </li>
		
        <li class="treeview">
          <a href="#">
            <i class="fa fa-building"></i>
            <span>Companies</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/companies/add') }}"><i class="fa fa-angle-double-right"></i> Add</a></li>
            <li><a href="{{ url('/companies/list') }}"><i class="fa fa-angle-double-right"></i> List</a></li>
          </ul>
        </li> -->

        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Users</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('add_user') }}"><i class="fa fa-angle-double-right"></i> Add User</a></li>
            <li><a href="{{ route('all_users') }}"><i class="fa fa-angle-double-right"></i> All Users</a></li>
            <li><a href="{{ route('add_role') }}"><i class="fa fa-angle-double-right"></i> User Role</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$title}}
        <!--<small>Control panel</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{$title}}</li>
      </ol>
    </section>