
@include('backend.partials.header')

@include('backend.partials.sidebar')
  
<section class="content">

	<div class="row">
		<div class="col-xs-12 white-box">
			@if (session()->has('message'))
				<div class="alert alert-info">
					{{ session('message') }}
				</div>
			@endif
		</div>
		
		

	</div>

	@include($file)

</section>

@include('backend.partials.footer')