<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Admin Routes
Route::get('admin', function () {
    return view('backend.login');
})->name('login');

Route::post('admin/login', 'LoginController@login');
Route::get('logout', 'LoginController@logout')->name('logout');

Route::middleware(['RedirectLogin'])->group(function () {

	Route::get('admin/dashboard','LoginController@dashboard')->name('dashboard');

	//Dealer Routes
	Route::get('admin/dealers/add', 'DealerController@add')->name('add_dealer');
	Route::post('admin/dealers/add', 'DealerController@insert_dealer');
	Route::get('admin/dealers/all', 'DealerController@all')->name('all_dealer');

	//Service Routes
	Route::get('admin/dealers/add-service','DealerController@add_service')->name('add_service');
	Route::post('admin/dealers/add-service','DealerController@insert_service');
	Route::get('admin/dealers/edit-service/{id}', 'DealerController@edit_service')->name('edit_service');
	Route::post('admin/dealers/edit-service/{id}', 'DealerController@update_service');

	//Experty Routes
	Route::get('admin/dealers/add-experty','DealerController@add_experty')->name('add_experty');
	Route::post('admin/dealers/add-experty','DealerController@insert_experty');
	Route::get('admin/dealers/edit-experty/{id}', 'DealerController@edit_experty')->name('edit_experty');
	Route::post('admin/dealers/edit-experty/{id}', 'DealerController@update_experty');

	//Facility Routes
	Route::get('admin/dealers/add-facility','DealerController@add_facility')->name('add_facility');
	Route::post('admin/dealers/add-facility','DealerController@insert_facility');
	Route::get('admin/dealers/edit-facility/{id}', 'DealerController@edit_facility')->name('edit_facility');
	Route::post('admin/dealers/edit-facility/{id}', 'DealerController@update_facility');

	//User Role Routes
	Route::get('admin/roles/add','UserController@add_role')->name('add_role');
	Route::post('admin/roles/add', 'UserController@insert_role');
	Route::get('admin/roles/edit/{id}', 'UserController@edit_role')->name('edit_role');
	Route::post('admin/roles/edit/{id}', 'UserController@update_role');
	Route::get('admin/roles/delete/{id}', 'UserController@delete_role')->name('delete_role');

	//User Routes
	Route::get('admin/users/add', 'UserController@add')->name('add_user');
	Route::post('admin/users/add', 'UserController@insert_user');
	Route::get('admin/users/list', 'UserController@all_users')->name('all_users');
	Route::get('admin/users/edit/{id}', 'UserController@edit_user')->name('edit_user');
	Route::post('admin/users/edit/{id}', 'UserController@update_user');
	Route::get('admin/users/delete/{id}', 'UserController@delete_user')->name('delete_user');

});

// Front End Routes
Route::get('/', function () {
    return view('welcome');
});