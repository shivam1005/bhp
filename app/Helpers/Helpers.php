<?php
//app/Helpers/Envato/User.php
namespace App\Helpers;
 
use Illuminate\Support\Facades\DB;
 
class Helpers {
	
    public static function get_info($tbl, $id){
    	$data = DB::table($tbl)->where('id', $id)->first();

    	return (isset($data) ? $data : '');
	}
	
    public static function get_all_info($tbl){
        $data = DB::table($tbl)->where('status', 1)->get();
		
        return (isset($data) ? $data : '');
    }
	
	public static function get_pilot($tbl){
        $data = DB::table($tbl)->where('status', 1)->where('role_id', 3)->get();

        return (isset($data) ? $data : '');
    }
	
	public static function encrypt($sData){
		$id=(double)$sData*145236.67;
		return base64_encode($id);
	}
	
	public static function decrypt($sData){
		$url_id=base64_decode($sData);
		$id=(double)$url_id/145236.67;
		return $id;
	}
	
    // public static function count_users(){
    //     $data = DB::table('users')->where('status', 1)->get();
    //     return (isset($data) ? count($data) : '');
    // }
	
	
	
}
