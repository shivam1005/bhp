<?php

namespace App\Http\Controllers; 

use App\Models\Service;
use App\Models\Experty;
use App\Models\Facility;
use App\Models\Dealer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;
use App\Helpers\Helpers;

class DealerController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function add(Request $request)
    {
        $file = 'backend/dealer/add';
        $title = 'Add New Dealer'; 

        return view('backend.template', compact('file', 'title') );
    }

    public function insert_dealer(Request $request)
    {
        $request->validate([
            'serive_center_name' => 'required',
            'phone' => 'required'
        ]);

        $data = new Dealer;

        $data->service_center_name = $request->input('serive_center_name');
        $data->phone = $request->input('phone');
        $data->address = $request->input('address');
        $data->city = $request->input('city');
        $data->state = $request->input('state');
        $data->pincode = $request->input('pincode');
        $data->location = $request->input('location');

        $data->authorized_for = $request->input('authorized');
        //$data->certificate = $request->input('service');
        $data->services = $request->input('services');
        $data->facilities = $request->input('facilities');
        $data->experties = $request->input('experties');

        //$data->photo = $request->input('service');

        $data->status = 1;
        $data->created_by = $request->session()->get('id');

        $data->save();

        return redirect()->route('add_dealer');
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function add_service(Request $request)
    {
        $data = Service::get();

        $file = 'backend/dealer/add_service';
        $title = 'Add New Service'; 

        return view('backend.template', compact('file', 'title', 'data') );
    }

    public function insert_service(Request $request)
    {
        $request->validate([
            'service' => 'required'
        ]);

        $data = new Service;

        $data->service_name = $request->input('service');
        $data->status = 1;
        $data->created_by = $request->session()->get('id');

        $data->save();

        return redirect()->route('add_service');
    }

    public function edit_service(Request $request, $id)
    {
        $id = Helpers::decrypt($id);
        $data = Service::where('id', $id)->first();

        $file = 'backend/dealer/edit_service';
        $title = 'Edit Service';

        return view('backend.template', compact('file', 'title', 'data') );
    }

    public function update_service(Request $request, $id)
    {
        $id = Helpers::decrypt($id);
        $request->validate([
            'service' => 'required'
        ]);

        $data = Service::find($id);

        $data->service_name = $request->input('service');
        $data->updated_by = $request->session()->get('id');

        $data->save();

        return redirect()->route('add_service')->with('message', 'Service has been updated.');
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function add_experty(Request $request)
    {
        $data = Experty::get();

        $file = 'backend/dealer/add_experty';
        $title = 'Add New Experty'; 

        return view('backend.template', compact('file', 'title', 'data') );
    }

    public function insert_experty(Request $request)
    {
        $request->validate([
            'experty' => 'required'
        ]);

        $data = new Experty;

        $data->expert_in = $request->input('experty');
        $data->status = 1;
        $data->created_by = $request->session()->get('id');

        $data->save();

        return redirect()->route('add_experty');
    }

    public function edit_experty(Request $request, $id)
    {
        $id = Helpers::decrypt($id);
        $data = Experty::where('id', $id)->first();

        $file = 'backend/dealer/edit_experty';
        $title = 'Edit Experty';

        return view('backend.template', compact('file', 'title', 'data') );
    }

    public function update_experty(Request $request, $id)
    {
        $id = Helpers::decrypt($id);
        $request->validate([
            'experty' => 'required'
        ]);

        $data = Experty::find($id);

        $data->expert_in = $request->input('experty');
        $data->updated_by = $request->session()->get('id');

        $data->save();

        return redirect()->route('add_experty')->with('message', 'Expert has been updated.');
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function add_facility(Request $request)
    {
        $data = Facility::get();

        $file = 'backend/dealer/add_facility';
        $title = 'Add New Facility'; 

        return view('backend.template', compact('file', 'title', 'data') );
    }

    public function insert_facility(Request $request)
    {
        $request->validate([
            'facility' => 'required'
        ]);

        $data = new Facility;

        $data->facility_name = $request->input('facility');
        $data->status = 1;
        $data->created_by = $request->session()->get('id');

        $data->save();

        return redirect()->route('add_facility');
    }

    public function edit_facility(Request $request, $id)
    {
        $id = Helpers::decrypt($id);
        $data = Facility::where('id', $id)->first();

        $file = 'backend/dealer/edit_facility';
        $title = 'Edit Facility';

        return view('backend.template', compact('file', 'title', 'data') );
    }

    public function update_facility(Request $request, $id)
    {
        $id = Helpers::decrypt($id);
        $request->validate([
            'facility' => 'required'
        ]);

        $data = Facility::find($id);

        $data->facility_name = $request->input('facility');
        $data->updated_by = $request->session()->get('id');

        $data->save();

        return redirect()->route('add_facility')->with('message', 'Facility has been updated.');
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //   public function insert_user(Request $request)
  //   {
  //       $request->validate([
  //           'role_id' => 'required',
  //           'email' => 'required|email|unique:users',
  //           'password' => 'required',
  //           'first_name' => 'required',
  //           'last_name' => 'required',
  //           'phone' => 'required',
  //           //'image' => 'image|mimes:jpeg,png,jpg|max:2048',
  //       ]);

  //       $user = new User; 
        
  //      	$user->role_id = $request->input('role_id');
  //       $user->first_name = $request->input('first_name');
  //       $user->last_name = $request->input('last_name');
  //       $user->email = $request->input('email');
  //       $user->phone = $request->input('phone');
  //       $user->password = md5($request->input('password'));
  //       $user->created_by = $request->session()->get('id');

  //       $user->save();

  //       return redirect()->route('add_user')->with('message', 'User has been Created.');    
  //   }

  //   public function all_users(){
  //       $users = User::get();

  //       $file = 'backend/user/all_users';
  //       $title = 'Users List';

  //       return view('backend.template', compact('file', 'title', 'users') );
  //   }

  //   public function edit_user(Request $request, $id)
  //   {
		// $id = Helpers::decrypt($id);
  //       $data = User::where('id', $id)->first();

  //       $file = 'backend/user/edit_user';
  //       $title = 'Edit User';

  //       return view('backend.template', compact('file', 'title', 'data') );
  //   }

  //   public function update_user(Request $request, $id)
  //   {
		// $id = Helpers::decrypt($id);
		// $request->validate([
  //           'role_id' => 'required',
  //           'email' => 'required|email|'.Rule::unique('users', 'email')->ignore($id),
  //           'first_name' => 'required',
  //           'last_name' => 'required',
  //           'phone' => 'required',
  //       ]);
		

  //      	$user = User::find($id);

  //       $user->role_id = $request->input('role_id');
  //       $user->first_name = $request->input('first_name');
  //       $user->last_name = $request->input('last_name');
  //       $user->email = $request->input('email');
  //       $user->phone = $request->input('phone');
  //       $user->updated_by = $request->session()->get('id');

  //       if( $request->input('password') ) {
  //           $user->password = md5($request->input('password'));
  //       } else {
  //           $user->password = $request->input('hidden_password');
  //       }
        
  //       $user->save();

  //       return redirect()->route('all_users')->with('message', 'User has been updated.'); 
  //   }

  //   public function delete_user(Request $request, $id)
  //   {
		// $id = Helpers::decrypt($id);
  //       $user = User::find($id);

  //       $user->status = '0';
  //       $user->updated_by = $request->session()->get('id');
        
  //       $user->save();

  //       return redirect()->route('all_users')->with('message', 'User has been removed.'); 
  //   }


  //   public function add_role(Request $request)
  //   {
		
  //       $data = Role::get();

  //       $file = 'backend/user/add_user_role';
  //       $title = 'Add New Role';

  //       return view('backend.template', compact('file', 'title', 'data') );
  //   }

  //   public function insert_role(Request $request)
  //   {
  //       $request->validate([
  //           'user_role' => 'required'
  //       ]);

  //      	$role = new Role;

  //       $role->role = $request->input('user_role');
  //       $role->permission = serialize($request->input('permission'));
  //       $role->status = 1;
  //       $role->created_by = $request->session()->get('id');

  //       $role->save();

  //       return redirect()->route('add_role');
  //   }

  //   public function edit_role(Request $request, $id)
  //   {
		// $id = Helpers::decrypt($id);
  //       $data = Role::where('id', $id)->first();

  //       $file = 'backend/user/edit_user_role';
  //       $title = 'Edit Role';

  //       return view('backend.template', compact('file', 'title', 'data') );
  //   }

  //   public function update_role(Request $request, $id)
  //   {
		// $id = Helpers::decrypt($id);
  //       $request->validate([
  //           'user_role' => 'required'
  //       ]);

  //       $role = Role::find($id);

  //       $role->role = $request->input('user_role');
  //       $role->permission = serialize($request->input('permission'));
  //       $role->updated_by = $request->session()->get('id');

  //       $role->save();

  //       return redirect()->route('add_role')->with('message', 'Role has been updated.');
  //   }

  //   public function delete_role(Request $request, $id)
  //   {
		// $id = Helpers::decrypt($id);
  //       $role = Role::find($id);

  //       $role->status = '0';
  //       $role->updated_by = $request->session()->get('id');

  //       $role->save();

  //       return redirect()->route('add_role')->with('message', 'Role has been removed.');
  //   }

}