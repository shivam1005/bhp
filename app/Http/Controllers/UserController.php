<?php

namespace App\Http\Controllers; 

use App\Models\User;
use App\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;
use App\Helpers\Helpers;

class UserController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function add(Request $request)
    {
        $file = 'backend/user/add';
        $title = 'Add New User'; 

        return view('backend.template', compact('file', 'title') );
    }

    public function insert_user(Request $request)
    {
        $request->validate([
            'role_id' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            //'image' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $user = new User; 
        
       	$user->role_id = $request->input('role_id');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->password = md5($request->input('password'));
        $user->created_by = $request->session()->get('id');

        $user->save();

        return redirect()->route('add_user')->with('message', 'User has been Created.');    
    }

    public function all_users(){
        $users = User::get();

        $file = 'backend/user/all_users';
        $title = 'Users List';

        return view('backend.template', compact('file', 'title', 'users') );
    }

    public function edit_user(Request $request, $id)
    {
		$id = Helpers::decrypt($id);
        $data = User::where('id', $id)->first();

        $file = 'backend/user/edit_user';
        $title = 'Edit User';

        return view('backend.template', compact('file', 'title', 'data') );
    }

    public function update_user(Request $request, $id)
    {
		$id = Helpers::decrypt($id);
		$request->validate([
            'role_id' => 'required',
            'email' => 'required|email|'.Rule::unique('users', 'email')->ignore($id),
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
        ]);
		

       	$user = User::find($id);

        $user->role_id = $request->input('role_id');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->updated_by = $request->session()->get('id');

        if( $request->input('password') ) {
            $user->password = md5($request->input('password'));
        } else {
            $user->password = $request->input('hidden_password');
        }
        
        $user->save();

        return redirect()->route('all_users')->with('message', 'User has been updated.'); 
    }

    public function delete_user(Request $request, $id)
    {
		$id = Helpers::decrypt($id);
        $user = User::find($id);

        $user->status = '0';
        $user->updated_by = $request->session()->get('id');
        
        $user->save();

        return redirect()->route('all_users')->with('message', 'User has been removed.'); 
    }


    public function add_role(Request $request)
    {
		
        $data = Role::get();

        $file = 'backend/user/add_user_role';
        $title = 'Add New Role';

        return view('backend.template', compact('file', 'title', 'data') );
    }

    public function insert_role(Request $request)
    {
        $request->validate([
            'user_role' => 'required'
        ]);

       	$role = new Role;

        $role->role = $request->input('user_role');
        $role->permission = serialize($request->input('permission'));
        $role->status = 1;
        $role->created_by = $request->session()->get('id');

        $role->save();

        return redirect()->route('add_role');
    }

    public function edit_role(Request $request, $id)
    {
		$id = Helpers::decrypt($id);
        $data = Role::where('id', $id)->first();

        $file = 'backend/user/edit_user_role';
        $title = 'Edit Role';

        return view('backend.template', compact('file', 'title', 'data') );
    }

    public function update_role(Request $request, $id)
    {
		$id = Helpers::decrypt($id);
        $request->validate([
            'user_role' => 'required'
        ]);

        $role = Role::find($id);

        $role->role = $request->input('user_role');
        $role->permission = serialize($request->input('permission'));
        $role->updated_by = $request->session()->get('id');

        $role->save();

        return redirect()->route('add_role')->with('message', 'Role has been updated.');
    }

    public function delete_role(Request $request, $id)
    {
		$id = Helpers::decrypt($id);
        $role = Role::find($id);

        $role->status = '0';
        $role->updated_by = $request->session()->get('id');

        $role->save();

        return redirect()->route('add_role')->with('message', 'Role has been removed.');
    }

}