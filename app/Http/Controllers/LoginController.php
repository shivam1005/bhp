<?php

namespace App\Http\Controllers;

use App\User;
//use App\Models\User;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    
    public function login(Request $request)
    {
        echo $email = $request->input('username');
        echo $password = $request->input('password');
        
        //$users = User::where('email', $id)->first();

        $users = DB::table('users')->where([
            ['email', '=', $email],
            ['password', '=', md5($password)],
            ['status', '=', 1],
        ])->first();

        if( $users ){
			if( $request->input('remember_me') == '1' )
			{
				$hour = time() + 3600 * 24 * 30;
				setcookie('username', $email, $hour);
				setcookie('password', $password, $hour);
				setcookie('remember_me', 1, $hour);
			}
            $request->session()->put('id', $users->id);
            $request->session()->put('role', $users->role_id);
            $request->session()->put('starttime', microtime());
            return redirect()->route('dashboard');
        }else{
            return redirect()->route('login')->with('message', 'Invalid User!');    
        }    
    }

    public function dashboard(Request $request)
    {
        $file = 'backend/dashboard'; 
        $title = 'Dashboard';

        return view('backend.template', compact('file', 'title') );
    }

    // public function forgot(Request $request)
    // {
    //     //$uid = $request->session()->get('id');
    //     //$user_data = User::where('id', $uid)->first();

    //     $file = 'forgot'; 
    //     $title = 'Forgot Password';

    //     return view('template', compact('file', 'title') );
    // }

    // public function check_email(Request $request)
    // {
    //     $request->validate([
    //         'email' => 'required',
    //     ]);

    //     $email = $request->input('email');

    //     $user_data = User::where('email', $email)->first();

    //     if( $user_data )
    //         {
    //             $uid = $user_data->id;    
    //             return view('reset_password', compact('uid') );
    //         }
    //     else
    //         {
    //             return redirect()->route('forgot_password')->with('message', 'This email id is not exist.');
    //         }
    // }

  //   public function logout(Request $request) {
        
  //       // $request->session()->forget('id'); 
  //       // $request->session()->forget('role');
		// $request->session()->flush();

  //       return redirect()->route('login');
  //   }

}