<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class RedirectLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
		$id = $request->session()->get('id');
        if ( empty($id)) {
            return redirect('/')->with('message', 'Session Timeout!');
        }
		else{
			$starttime = $request->session()->get('starttime');
			$startarray = explode(" ", $starttime);
			$starttime = $startarray[1] + $startarray[0];
			$totaltime = time() - $starttime;
			if ($totaltime > 10*60) {
				$request->session()->flush();
				return redirect('/')->with('message', 'Session Timeout!');
			}
		}
		
		// else {
		// 	$routename = Route::currentRouteName();
		// 	if($routename){
		// 		$roleid = $request->session()->get('role');
				
		// 		$user_role = Role::where('id', $roleid)->where('status', 1)->first();
		// 		if($user_role){
		// 			$permissions = unserialize( $user_role->permission );
					
		// 			if( in_array($routename, $permissions) ) {
		// 				return $next($request);
		// 			} else {
		// 				return redirect()->route('dashboard')->with('message', 'Sorry! You are not authorised for that url.'); 
		// 			}
					
		// 		} else {
		// 			$request->session()->forget('id'); 
		// 			return redirect('/');
		// 		}
		// 	} else {
		// 		return $next($request);
		// 	}
		// }
		
		$request->session()->put('starttime', microtime());
		
        return $next($request);
    }
}
